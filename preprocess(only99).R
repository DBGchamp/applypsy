pacman::p_load(tidyverse)
#####################################################
###	99 School Data
#####################################################
### under windows - convert data format from big-5 to UTF-8
# dta99 <- read.csv("99_undergraduate_big5.csv", h = TRUE, fileEncoding = "Big5")
# dim(dta99)
# write.csv(dta99, "99_SchoolData.csv", row.names = FALSE, fileEncoding = "UTF-8")

### Basic Information
dta99 <- read.csv("99_SchoolData.csv", h = TRUE, fileEncoding = "UTF-8")
dta99_Info <- dta99[,1:70]
dta99_Info <- dta99_Info[!duplicated(dta99_Info),]
names(dta99_Info)
levels(dta99_Info$交換學生之國家)[1] <- "NA"
dta99_Info$change <- ifelse(dta99_Info$交換學生之國家 == "NA", 0, 1)
dta99_Info[,9] <- as.character(dta99_Info[,9])
dta99_Info <- dta99_Info %>% 
    separate(交換學生之期間, c('出國年月', '回國年月'), sep = "\\-")
dta99_Info$dropout <- ifelse(dta99_Info$最近休學年.學期 == "NA", 0, 1)
dta99_Info <- dta99_Info[,c(1:7,72,8:10,73,11:71)]
names(dta99_Info) <- c("學號", "去識別碼", "學院", "系所", "學制","年級","入學年",
                       "申請交換","交換國家","出國年月","回國年月","申請休學",
                       "休學年/學期","復學年/學期","是否退學","申請輔系","申請雙主修",
                       "跨領域學程","申請延畢","性別","國籍","就讀高中","出生縣市",
                       "身分類別","入學管道","學測成績E","畢業門檻E","通過項目E",
                       "入學分級E","入學模組E","通過抵免E","抵免項目E","就學貸款",
                       "監護人職業","第一親屬職業","第二親屬職業",
                       paste0(rep(c("社團名稱","社團性質", "社團職務"),12),rep(1:12,each = 3)),
                       "核心能力總分")

### School Grade
dta99_Grade <- dta99[,c(2,26,71:83)]
names(dta99_Grade)
dta99_Grade$Withdraw <- ifelse(dta99_Grade$X.學生上課課程.課程分數 == "退選", 1, 0)
dta99_Grade$waive <- ifelse(dta99_Grade$X.學生上課課程.課程分數 == "抵免", 1, 0)
dta99_Grade <- dta99_Grade %>% mutate(X.教學反應問卷.綜評.整體而言.本科目教師的教學優良 = as.character(X.教學反應問卷.綜評.整體而言.本科目教師的教學優良), 
                                      X.學生上課課程.課程分數 = as.character(X.學生上課課程.課程分數))
dta99_Grade <- dta99_Grade %>% mutate(X.教學反應問卷.綜評.整體而言.本科目教師的教學優良 = as.numeric(X.教學反應問卷.綜評.整體而言.本科目教師的教學優良),
                                      X.學生上課課程.課程分數 = as.numeric(X.學生上課課程.課程分數))
names(dta99_Grade) <- c("去識別碼","入學分級E","修課學年","修課學期","學期排名",
                        "畢業排名","課程代碼","課程名稱","授課老師","授課教師Q",
                        "教學評鑑Q","學生出席Q","專心程度Q","受益良多Q","學期分數Q",
                        "學生退選","學生抵免")
dta99_Grade <- dta99_Grade %>% mutate(學期分數Q = as.numeric(as.character(學期分數Q)))

### 畢業問卷
dta99_question <- dta99[,c(2,84:321)]
names(dta99_question) <- c("去識別碼",paste0("自主實習", 1:5), paste0("職涯教練",1:33),
                           paste0("工作意向",1:55), paste0("畢業一年",1:70),
                           paste0("畢業三年",1:75))
dta99_question <- dta99_question[!duplicated(dta99_question),]

### 存檔
write.csv(dta99_Info, "99_Info.csv", row.names = FALSE, fileEncoding = "UTF-8")
write.csv(dta99_Grade, "99_Grade.csv", row.names = FALSE, fileEncoding = "UTF-8")
write.csv(dta99_question, "99_Question.csv", row.names = FALSE, fileEncoding = "UTF-8")

#####################################################
###	unused code
#####################################################

###	97 School Data
dta97 <- read.csv("97_SchoolData.csv", h = TRUE, fileEncoding = "Big5")
names(dta97)
dta97_grade <- dta97[,c(2,4,15,69:79)]

###	Grade
dta97_grade_names <- c("Serial_Num","Dept","Gender","academic_year",
	"Semester","Course_code","Course_name","Teacher","Teacher(Rating)",
	"Rate","Attendance","Attention","Acquire","Grade")
codebook_grade <- data.frame(dta97_grade_names ,names(dta97_grade))
names(codebook_grade) <- c("Var", "Meaning")
names(dta97_grade) <- dta97_grade_names
dta97_grade$Withdraw <- ifelse(dta97_grade$Grade == "退選", 1, 0)
dta97_grade$Offset <- ifelse(dta97_grade$Grade == "抵免", 1, 0)
dta97_grade <- dta97_grade %>% mutate(Rate = as.character(Rate), 
	Grade = as.character(Grade))
dta97_grade <- dta97_grade %>% mutate(Rate = as.numeric(Rate),
	Grade = as.numeric(Grade))

###	Basic Information
dta97_info <- dta97[,1:68]
dta97_info_names <- c("School_ID", "Serial_Num", "College", "Dept", 
	"Degree","Grade","Entry_year","Suspense","Re-Enroll","Drop-out",
	"Minor","Double_Major","Program","Postpone_Graduate","Gender",
	"Nationality","High_School","City","Disable","Entrance_way",
	"CCE_Eng","Eng_pass","Eng_pass_item","Eng_level_A","Eng_level_B",
	"Eng_Waive","Eng_Waive_item","College_loan","Guardian_job",
	"Kin1_job","Kin2_job", 
	paste0(rep(c("club","club_type", "club_job"),12),rep(1:12,each = 3)),
	"Kernal_abilit")
codebook_info <- data.frame(dta97_info_names, names(dta97_info))
names(codebook_info) <- c("Var", "Meaning")
names(dta97_info) <- dta97_info_names
dta97_info <- dta97_info[!duplicated(dta97_info),]

###	questionnire
dta97_question <- dta97[,c(2,80:317)]
dta97_question_names <- c("Serial_Num", paste0("Self_learn", rep(1:5)),
	paste0("Coach", rep(1:33)), paste0("Intense", rep(1:55)),
	paste0("Graduate_1_", rep(1:70)), paste0("Graduate_3_", rep(1:75)))
codebook_question <- data.frame(dta97_question_names, names(dta97_question))
names(codebook_question) <- c("Var", "Meaning")
names(dta97_question) <- dta97_question_names
dta97_question <- dta97_question[!duplicated(dta97_question),]

###	Codebook
codebook <- rbind(codebook_info, codebook_grade[-c(1:3),])
codebook <- rbind(codebook, codebook_question[-1,])

write.csv(codebook, "97_Codebook.csv", row.names = FALSE, fileEncoding = "Big5")
write.csv(dta97_info, "97_Info.csv", row.names = FALSE, fileEncoding = "Big5")
write.csv(dta97_grade, "97_Grade.csv", row.names = FALSE, fileEncoding = "Big5")
write.csv(dta97_question, "97_Questionnaire.csv", row.names = FALSE, fileEncoding = "Big5")

###	99 School Data
dta99 <- read.csv("99_SchoolData.csv", h = TRUE, fileEncoding = "Big5")
names(dta99)
dta99_grade <- dta99[,c(2,4,15,69:79)]

###	Grade
dta99_grade_names <- c("Serial_Num","Dept","Gender","academic_year",
	"Semester","Course_code","Course_name","Teacher","Teacher(Rating)",
	"Rate","Attendance","Attention","Acquire","Grade")
codebook_grade <- data.frame(dta99_grade_names ,names(dta99_grade))
names(codebook_grade) <- c("Var", "Meaning")
names(dta99_grade) <- dta99_grade_names
dta99_grade$Withdraw <- ifelse(dta99_grade$Grade == "退選", 1, 0)
dta99_grade$Offset <- ifelse(dta99_grade$Grade == "抵免", 1, 0)
dta99_grade <- dta99_grade %>% mutate(Rate = as.character(Rate), 
	Grade = as.character(Grade))
dta99_grade <- dta99_grade %>% mutate(Rate = as.numeric(Rate),
	Grade = as.numeric(Grade))

###	Basic Information
dta99_info <- dta99[,1:68]
dta99_info_names <- c("School_ID", "Serial_Num", "College", "Dept", 
	"Degree","Grade","Entry_year","Suspense","Re-Enroll","Drop-out",
	"Minor","Double_Major","Program","Postpone_Graduate","Gender",
	"Nationality","High_School","City","Disable","Entrance_way",
	"CCE_Eng","Eng_pass","Eng_pass_item","Eng_level_A","Eng_level_B",
	"Eng_Waive","Eng_Waive_item","College_loan","Guardian_job",
	"Kin1_job","Kin2_job", 
	paste0(rep(c("club","club_type", "club_job"),12),rep(1:12,each = 3)),
	"Kernal_abilit")
codebook_info <- data.frame(dta99_info_names, names(dta99_info))
names(codebook_info) <- c("Var", "Meaning")
names(dta99_info) <- dta99_info_names
dta99_info <- dta99_info[!duplicated(dta99_info),]

###	questionnire
dta99_question <- dta99[,c(2,80:317)]
dta99_question_names <- c("Serial_Num", paste0("Self_learn", rep(1:5)),
	paste0("Coach", rep(1:33)), paste0("Intense", rep(1:55)),
	paste0("Graduate_1_", rep(1:70)), paste0("Graduate_3_", rep(1:75)))
codebook_question <- data.frame(dta99_question_names, names(dta99_question))
names(codebook_question) <- c("Var", "Meaning")
names(dta99_question) <- dta99_question_names
dta99_question <- dta99_question[!duplicated(dta99_question),]

###	Codebook
codebook <- rbind(codebook_info, codebook_grade[-c(1:3),])
codebook <- rbind(codebook, codebook_question[-1,])

write.csv(codebook, "99_Codebook.csv", row.names = FALSE, fileEncoding = "Big5")
write.csv(dta99_info, "99_Info.csv", row.names = FALSE, fileEncoding = "Big5")
write.csv(dta99_grade, "99_Grade.csv", row.names = FALSE, fileEncoding = "Big5")
write.csv(dta99_question, "99_Questionnaire.csv", row.names = FALSE, fileEncoding = "Big5")