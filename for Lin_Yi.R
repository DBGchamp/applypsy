########################################
### fill in the questionnire or not
########################################
### packages and functions
# use packages for Cramer's V
library(lsr)

# function 1
vplayout <- function(x, y) viewport(layout.pos.row = x, layout.pos.col = y)

# function 2
se <- function(input) {
    if(sum(is.na(input)) > 0) { warning("The NAs had been remove!") }
    sd(input, na.rm = TRUE)/sqrt(length(input) - sum(is.na(input)))
}

### read data
df <- read.csv("10_vars.csv", h = T, fileEncoding = "UTF-8")

### table (need chisq)
# example
club <- table(df$填寫問卷, df$參與社團)
chisq.test(table(df$填寫問卷, df$參與社團))
cramersV(club)

# 還需要做 (英文門檻 學院 跨領域學習 重要幹部 就學貸款 rank)
# 請利用上述的code改寫，若遇到 warning 時,在chisq.test中
# 加 simulate.p.value = TRUE, B = 20000
# 結果幫我用 powerpoint 呈現 X-squared, df, p-value, cramersV
# 感謝。